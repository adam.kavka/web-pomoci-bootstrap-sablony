<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Přihlášení uživatele</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-navbar">
            <div class="container px-5">
                <a class="navbar-brand" href="#!">Start Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <?php
                            include "navigace.php";
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page Content-->
        <h1 class="mt-5">Přihlášení uživatele</h1>
        <form action="login-script.php" method="post">
                <div class="mb-3">
                    <label for="login" class="form-label">login (*)</label>
                    <input type="text" class="form-control" name="login" id="login"  placeholder="Váš login" required>
                </div>
                <div class="mb-3">
                    <label for="heslo1" class="form-label">heslo (*)</label>
                    <input type="password" class="form-control" name="heslo1" id="heslo1" required>
                </div>
                
                
                <input type="submit" value="Přihlásit se" name="submit" class="btn btn-primary">
                <br><br>
            </form>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
