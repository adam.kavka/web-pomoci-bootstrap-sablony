<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Přihlášení uživatele</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/main.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-navbar">
            <div class="container px-5">
                <a class="navbar-brand" href="#!">Start Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <?php
                            include "navigace.php";
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Page Content-->
        <h1 class="mt-5">Přihlášení uživatele</h1>
        <?php
          // Nastavení proměnných pro připojení k databázi
          $hostName = "localhost";
          $databaseName = "adam.kavka";
          $userName = "root";
          $password = "";

          // Připojení k MySQL/MariaDB serveru
          $idSpojeni = mysqli_connect($hostName,$userName,$password);

          // Připojení k DB
          $idDB = mysqli_select_db($idSpojeni, $databaseName);

          session_start(); // zahájení uživatelské relace  

          $login = $_POST["login"];
          $heslo1 = sha1($_POST["heslo1"]);
          
          // Ověření, zda uživatel v DB existuje
          $sqlDotaz = "SELECT * FROM users WHERE login='$login' AND heslo1='$heslo1'";
          $dotazUzivatel = mysqli_query($idSpojeni, $sqlDotaz);
          
          $pocetUzivatel = mysqli_num_rows($dotazUzivatel);
          printf("<p>Počet vyhledaných záznamů: $pocetUzivatel");          
          
          $detailUzivatel = mysqli_fetch_array($dotazUzivatel);
          
          if($pocetUzivatel==1) {
            $_SESSION['loggedUser'] = $login;
            $_SESSION['admin'] = $detailUzivatel['admin'];
            printf("<p>Uživatel: <b>" .$_SESSION['loggedUser']. "</b> byl v DB nalezen.</p>");
            printf("<p>Admin: <b>" .$_SESSION['admin']. "</b></p>");    
            printf("<p><a href='index.php'>Pokračovat na úvodní stránku</a></p>");
          }
          else {
            printf("<p>Uživatel NEBYL v databázi nalezen, nebo jste zadali špatné heslo.</p>");
            printf("<p><a href='login-form.php'>Znovu se přihlásit</a></p>");
          }


          mysqli_close($idSpojeni);

        ?>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
