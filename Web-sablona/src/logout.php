	<?php
          // Nastavení proměnných pro připojení k databázi
          $hostName = "localhost";
          $databaseName = "adam.kavka";
          $userName = "root";
          $password = "";

          // Připojení k MySQL/MariaDB serveru
          $idSpojeni = mysqli_connect($hostName,$userName,$password);

          // Připojení k DB
          $idDB = mysqli_select_db($idSpojeni, $databaseName);

          session_start(); // zahájení uživatelské relace  
          
          if(isset($_SESSION['loggedUser'])) {
            $login = $_SESSION['loggedUser'];
            unset($_SESSION['loggedUser']);
            printf("<p>Uživatel <b>$login</b> byl odhlášen.</p>");
            printf("<p>Pokračovat na <a href='index.php'>úvodní stránku</a></p>");
          }
          else
            printf("<p>Nikdo nebyl přihlášen, tak není koho odhlásit.</p>");  
          
          mysqli_close($idSpojeni);

        ?>